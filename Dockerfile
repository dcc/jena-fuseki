FROM ubuntu:20.04
LABEL project="SPHN"
LABEL version="alpha"

RUN apt-get update -qy && apt-get install -y \
	default-jre

COPY apache-jena-fuseki-3.17.0.tar.gz /terminology_server/
COPY load.sh /terminology_server/
COPY tdbloader /terminology_server/
COPY docker-entrypoint.sh /terminology_server/
COPY shiro.ini /terminology_server/
# Copying over the data, when building the containter, this is a workaround when running the container on an environment where the data mount does not exist.
COPY Dockerfile ./data/* /data/


WORKDIR /terminology_server 
RUN chmod 755 *
RUN tar -xzf apache-jena-fuseki-3.17.0.tar.gz

# Environments 
VOLUME /data
ENV JVM_ARGS=-Xmx2g
ENV FUSEKI_BASE=/fuseki-data
ENV FUSEKI_HOME=/apache-jena-fuseki
ENV DATA_LOAD_DIR=/data
ENV SCRIPT_DIR=/terminology_server

RUN mv /terminology_server/apache-jena-fuseki-3.17.0 $FUSEKI_HOME && mkdir $FUSEKI_BASE && mkdir $FUSEKI_BASE/databases && cp $SCRIPT_DIR/shiro.ini $FUSEKI_BASE/

EXPOSE 3030 
ENTRYPOINT ["./docker-entrypoint.sh"]
CMD ["fuseki"]
