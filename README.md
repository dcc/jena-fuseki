# Apache Jena Fuseki
This repository contains resources to be able to start an Apache Jena Fuseki based on version 3.17.0. Some resources are copied and modified from the original sources. To not interfere with the license, the license stays the same.
Apache Jena is licensed using the Apache 2.0 License. Documentation about Apache Jena can be found here: https://jena.apache.org/

## Installation
The following exemplary steps shows how to run Apache Jena Fuseki based on a Ubuntu 20.04 installation.
It assumes you are root (only necessary for the first two statements) and that you have checked out the git project to the directory you are currently working on.
```
    - apt-get update -qy
    - apt-get install -y default-jre lftp
    - tar -xzvf terminology_server/apache*.tar.gz
    - export JVM_ARGS=-Xmx2g
    - export FUSEKI_BASE=$PWD/fuseki-data
    - export FUSEKI_HOME=$PWD/apache-jena-fuseki
    - export DATA_LOAD_DIR=$PWD/data
    - export SCRIPT_DIR=$PWD/terminology_server
    - mv apache-jena-fuseki-3.17.0 $FUSEKI_HOME
    - mkdir $FUSEKI_BASE
    - mkdir $FUSEKI_BASE/databases
    - chmod -R 755 terminology_server/
    - chmod -R 755 data
    # adding some debug
    - ls -lsh
    - ls -lsh data
    - terminology_server/load.sh terminologies
```
Then you can start the terminology service with:
```
$FUSEKI_HOME/fuseki-server --loc=$FUSEKI_BASE/databases/terminologies /terminologies
```

## Dockerized Runtime
You can also run Fuseki with the content as a docker container.
The terminology_server directory contains the neccessary files and contents.

Have docker installed and be allowed to run it.
Download the content of the "terminology_server" content
Download the data to <terminology_data_folder>
Run the below two commands:
```
docker build -t sphn-fuseki .

docker run -v <terminology_data_folder>:/data -p 3030:3030 -d sphn-fuseki
```
Fuseki is then available using localhost:3030 with the terminologies preloaded.
You can browse it with a webbrowser or query it with SPARQL-Federation with your favorite triplestore.

## Testing
You can test the installation and the loading of the triples with this query:
```
curl -X POST -d "query=SELECT (COUNT (*) as ?numtriples) WHERE {?s ?p ?o.}" http://localhost:3030/terminologies/sparql
```

## WebInterface / Updates
The webinterface is also rechable using a webbrowser and port 3030.
The webinterface as well as updating datasets requires a username and password. This is set in the shiro.ini file during building the container or running the fuseki jar.
By default this is set to:
user: admin
pass: pw


## License
Licensed under the Apache License, Version 2.0.