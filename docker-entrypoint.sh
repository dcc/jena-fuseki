#!/bin/bash
set -e

if [ "$1" = 'fuseki' ]; then
    $SCRIPT_DIR/load.sh terminologies
    exec $FUSEKI_HOME/fuseki-server --loc=$FUSEKI_BASE/databases/terminologies /terminologies
else
    exec "$@"
fi


